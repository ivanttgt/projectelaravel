<h1> Tots els Objectes</h1>
@if(count($find)>0)
	<ul>
		@foreach($find as $element)
		<li>Nom: {{ $element->Nom}}
        Nivell Mínim: {{ $element->Minlvl}}
        Qualitat: {{$element->qualitat}}
        Categoria: {{$element->categoria}}
        Imatge: {{$element->imgpath}}
    </li>

		@endforeach
	</ul>
@else
	<li>No hi ha elements</li>
@endif
