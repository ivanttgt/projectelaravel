@extends('layouts.app', ['activePage' => 'register', 'title' => 'All Objects'])
@section('content')
        <div class="container"> 
            <a class="btn btn-primary" href="myObjects" role="button">MyObjects</a>
            <a class="btn btn-primary" href="Auctions" role="button">Auctions</a>
            <a class="btn btn-primary" href="bids" role="button">Bids</a>
            <a class="btn btn-primary" href="allObjects" role="button">All Objects</a>
        </div>
        <div class="container">
            <div class="row">
		          @if(count($find)>0)
                    <h1> Tots els Objectes</h1>
			             <ul>
                             @foreach($find as $element)
                             <li>Nom: {{ $element->Nom}},
                                 Nivell Mínim: {{ $element->Minlvl}},
                                 Qualitat: {{$element->qualitat}},
                                 Categoria: {{$element->categoria}},
                                 Imatge: {{$element->imgpath}}
                             </li>

				            @endforeach
			             </ul>
                    @else
                        <li>No hi ha elements</li>
		          @endif
          </div>
</div>
@endsection
@section('footer')
	<h4>M6 Acces a Dades</h4>
@endsection
