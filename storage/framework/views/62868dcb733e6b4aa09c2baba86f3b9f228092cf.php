<html>
	<head>
		<title>Prova1</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<h1>Tots els Objectes</h1>
					<?php echo $__env->yieldContent('content'); ?>
				</div>
			</div>
		</div>
		<footer>
			<?php echo $__env->yieldContent('footer'); ?>
		</footer>
	</body>
</html>
<?php /**PATH C:\Users\sergi\LaravelProjecte\projectelaravel\resources\views/allObjectshtml.blade.php ENDPATH**/ ?>