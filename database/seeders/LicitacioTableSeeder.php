<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Licitacio;

class LicitacioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Licitacions')->insert([
            'PreuOfert' => '5000',
            'Licitari' => 'Hermototem',
            'DataLicitacio' => now()
        ]);
    }
}
