<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Usuaris', function (Blueprint $table) {
            $table->id();
            $table->string('Nom', 255);
            $table->string('Correu', 255);
            $table->string('password');
            $table->integer('LicitacioMax');
            $table->string('rol');
            $table->integer('or');
            $table->foreignId('idObjecte')
                ->references('id')
                ->on('Objectes');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Usuari');
    }
}
