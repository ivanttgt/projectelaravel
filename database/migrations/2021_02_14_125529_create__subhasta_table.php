<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubhastaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Subhasta', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idJugador')
                ->references('id')
                ->on('Usuaris');
            $table->foreignId('idObjecteSubhastat')
                ->references('id')
                ->on('Objectes');
            $table->integer('LicitacioMax');
            $table->integer('PreuInstantani');
            $table->timestamp('DataFi');
            $table->boolean('SubhastaActiva');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Subhasta');
    }
}
