<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicitacioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Licitacions', function (Blueprint $table) {
            $table->id();
			$table->integer('PreuOfert');
            $table->foreignId('idLicitari')->constrained('Usuaris');
            $table->timestamp('Data');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Licitacio');
    }
}
