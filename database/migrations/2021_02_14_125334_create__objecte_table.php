<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjecteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('Objectes', function (Blueprint $table) {
            $table->id();
            $table->string('Nom', 255);
            $table->integer('Minlvl');
            $table->string('qualitat', 255);
            $table->string('categoria', 255);
            $table->string('imgpath', 500);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Objecte');
    }
}
