<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Licitacio;
use App\Models\Objecte;
use App\Models\Usuari;
use App\Models\Subhasta;
//use App\Models\Enums;

class MainController extends Controller
{
     public function show_auctions($id) {
        $find = Subhasta::all($id);
        return $find->toJson();
    }

    public function show_Objectes_usuari($id) {
        $find = Objecte::all($id);
        return $find->toJson();
    }

     public function show_AllObjects() {
        $find = Objecte::all();
        return view('allObjects', compact('find'));
    }

    public function show_bids($id) {
        $find = Bid::all($id);
        return $find->toJson();
    }

    public function create_bid() {

    }

    public function create_auction() {

    }

    public function fer_licitacio() {

    }

    public function subhastes_exhaurides() {

    }

    public function new_object(Request $_request, $_Nom, $_Minlvl, $_qualitat, $_categoria, $_imgpath) {
        $data = [
            'Nom' =>  $_Nom,
            'Minlvl' => $_Minlvl,
            'qualitat' => $_qualitat,
            'categoria' => $_categoria,
            'imgpath' => $_imgpath
        ];
        $tots=Objecte::create($data);
        return $tots->toJson();
    }
}
