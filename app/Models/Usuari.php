<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuari extends Model
{
    use HasFactory;
    protected $table = 'Usuaris';
    protected $primarykey = 'ID';
    protected $fillable = ['ID','Nom','Correu','Contrasenya','rol','or','objectes'];

    public function subhasta()
    {
        return $this->hasMany(Subhasta::class);
    }

    public function objecte()
    {
        return $this->hasMany(Objecte::class);
    }

    public function licitacio()
    {
        return $this->hasMany(Licitacio::class);
    }
}
