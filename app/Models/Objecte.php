<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objecte extends Model
{
   use HasFactory;
   //use Enums;

   /*protected $qualitat = [
      'Comu', 
      'Poc Comu', 
      'Rar', 
      'Epic', 
      'Llegendari'
   ];*/

   protected $table = 'Objectes';
   protected $primarykey = 'id';
   protected $fillable = ['id','Nom','Minlvl', 'qualitat', 'categoria','imgpath'];

   public function usuari(){
      return $this->belongsTo(Usuari::class);
   }

   public function subhasta(){
      return $this->belongsTo(subhasta::class);
   }
}
