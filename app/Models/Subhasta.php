<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subhasta extends Model
{
   use HasFactory;
   protected $table = 'Subhastes';
   protected $primarykey = 'ID';
   protected $fillable = ['ID','Subhastant','ObjSubhastat','LicitacioMax','PreuInstantani','DataFi','SubhastaActiva'];

   public function usuari(){
      return $this->belongsTo(Usuari::class);
   }

   public function licitacio(){
      return $this->belongsTo(Licitacio::class);
   }

   public function objecte(){
      return $this->hasOne(Objecte::class);
   }
}
