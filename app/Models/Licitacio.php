<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Licitacio extends Model
{
   use HasFactory;
   protected $table = 'Licitacions';
   protected $primarykey = 'id';
   protected $fillable = ['id','PreuOfert','idLicitari','Data'];

   public function subhasta()
    {
         return $this->hasMany(Subhasta::class);
    }

    public function usuari()
    {
         return $this->belongsTo(Usuari::class);
    }
}
