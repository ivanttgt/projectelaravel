<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('login');

Auth::routes();

/*fet*/
Route::get('/auctions', [App\Http\Controllers\MainController::class, 'show_auctions']);

Auth::routes();

/*fet*/
Route::get('/bids', [App\Http\Controllers\MainController::class, 'show_bids']);

Auth::routes();

Route::get('/bids/newBid', [App\Http\Controllers\MainController::class, 'create_bid']);

Auth::routes();

Route::get('/auction', [App\Http\Controllers\MainController::class, 'create_auction']);

Auth::routes();

Route::get('/bid/idsubhasta', [App\Http\Controllers\MainController::class, 'fer_Licitacio']);

Auth::routes();

Route::get('/check', [App\Http\Controllers\MainController::class, 'subhastes_exhaurides']);

Auth::routes();

/*fet*/
Route::get('/myObjects', [App\Http\Controllers\MainController::class, 'show_Objectes_usuari']);

Auth::routes();

Route::get('/myObjects/newObject/{_Nom}/{_Minlvl}/{_qualitat}/{_categoria}/{_imgpath}', [App\Http\Controllers\MainController::class, 'new_object']);

Auth::routes();

Route::get('/allObjects', [App\Http\Controllers\MainController::class, 'show_AllObjects']);

Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::patch('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::patch('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

